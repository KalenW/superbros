package com.mygdx.game.superbros;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class GameController {
    Viewport viewport;
    Stage stage;
    boolean JUMP, LEFT, RIGHT;
    OrthographicCamera cam;

    public GameController(){
        cam = new OrthographicCamera();
        viewport = new FitViewport(400, 208, cam);
        stage = new Stage(viewport, SuperBros.batch);

        stage.addListener(new InputListener(){



            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                switch(keycode)
                {
                    case Input.Keys.RIGHT:
                        RIGHT = false;
                        break;

                    case Input.Keys.LEFT:
                        LEFT = false;
                        break;

                    case Input.Keys.UP:
                        JUMP = false;
                        break;

                }
                return true;
            }
        });

        Gdx.input.setInputProcessor(stage);

        Table table = new Table();
        table.left().bottom();

        Image JumpImage = new Image(new Texture("jump_button.png"));
        JumpImage.setSize(25, 25);
        JumpImage.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                JUMP = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                JUMP = false;
            }
        });

        Image RIGHTImage = new Image(new Texture("RightButton.png"));
        RIGHTImage.setSize(25, 25);
        RIGHTImage.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                RIGHT = true;
                return true;
            }

            @Override
             public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                RIGHT = false;
            }
        });

        Image LEFTImage = new Image(new Texture("LeftButton.png"));
        LEFTImage.setSize(25, 25);
        LEFTImage.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                LEFT = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                LEFT = false;
            }
        });

        JumpImage.setPosition(350, 20);
        stage.addActor(JumpImage);

        table.add();
        table.add();
        table.row().pad(10, 10, 10, 10);
        table.add(LEFTImage).size(LEFTImage.getWidth(), LEFTImage.getHeight());
        table.add();
        table.add(RIGHTImage).size(RIGHTImage.getWidth(), RIGHTImage.getHeight());
        table.row().padBottom(10);
        table.add();

        stage.addActor(table);
    }

    public void draw(){
        stage.draw();
    }

    public boolean isJUMP() {
        return JUMP;
    }

    public boolean isLEFT() {
        return LEFT;
    }

    public boolean isRIGHT() {
        return RIGHT;
    }

    public void resizeController(int width, int height){
        viewport.update(width, height);
    }
}
