package com.mygdx.game.superbros;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.superbros.SuperBros;



/**
 * Created by Kalen Wood-Wardlow on 11/27/2016.
 */

/*
 *HUD: creates the static HUD to display basic game info
 */
public class HUD implements Disposable
{
    public Stage stage;
    private Viewport viewport;

    private Integer timer;
    private float time;
    private static Integer score;

    Label countdownLabel;
    static Label scoreLabel;
    Label timeLabel;
    Label levelLabel;
    Label worldLabel;
    Label brosLabel;


    public HUD(SpriteBatch sb)
    {
        timer = 300;
        time = 0;
        score = 0;

        viewport = new FitViewport(SuperBros.V_WIDTH, SuperBros.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);
        Table table = new Table();
        table.top();
        table.setFillParent(true);

        countdownLabel = new Label(String.format("%03d", timer), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        scoreLabel = new Label(String.format("%06d", score), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeLabel = new Label("TIME", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        levelLabel = new Label("BROS", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        worldLabel = new Label("SUPER", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        brosLabel = new Label("Score", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        table.add(brosLabel).expandX().padTop(10);
        table.add(worldLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.row();
        table.add(scoreLabel).expandX();
        table.add(levelLabel).expandX();
        table.add(countdownLabel).expandX();

        stage.addActor(table);
    }


    @Override
    public void dispose() {
        stage.dispose();

    }

    public static void addToScore(int val)
    {
        score += val;
        scoreLabel.setText(String.format("%06d", score));
    }

    public void update(float dt)
    {
        time += dt;
        if(time >= 1)
        {
            timer --;
            countdownLabel.setText(String.format("%03d", timer));
            time = 0;
        }
    }
}
