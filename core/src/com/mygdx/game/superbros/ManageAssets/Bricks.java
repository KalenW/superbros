package com.mygdx.game.superbros.ManageAssets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.superbros.HUD;
import com.mygdx.game.superbros.Screens.PlayScreen;
import com.mygdx.game.superbros.SuperBros;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class Bricks extends InteractableTileObjects {

    public Bricks(PlayScreen playScreen, Rectangle bounds)
    {
        super(playScreen, bounds);
        fixture.setUserData(this);
        setCatFilter(SuperBros.bricksBit);
    }

    @Override
    public void headHit() {
        Gdx.app.log("Bricks", "Collison");
        setCatFilter(SuperBros.destroyBit);
        getCellSpace().setTile(null);
        HUD.addToScore(100);
    }
}
