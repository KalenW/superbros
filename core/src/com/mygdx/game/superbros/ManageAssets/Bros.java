package com.mygdx.game.superbros.ManageAssets;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.superbros.Screens.PlayScreen;
import com.mygdx.game.superbros.SuperBros;


/**
 * Created by Kalen Wood-Wardlow.
 */

public class Bros   extends Sprite {
    public World world;
    public Body box2Bod;
    private TextureRegion broStand;
    private Animation broRun;
    private Animation broJump;
    private boolean runRight;
    private float stateTimer;

    //Create different states of bros
    public enum State{FALLING, JUMPING, STANDING, RUNNING};
    public State currState; //Current state
    public State prevState; //Previous state


    public Bros(PlayScreen screen)
    {
        super(screen.getAtlas().findRegion("little_bro"));
        this.world = screen.getTheWorld();
        currState = State.STANDING;
        prevState = State.STANDING;
        stateTimer = 0;
        runRight = true;
        Array<TextureRegion> frames = new Array<TextureRegion>();

        for(int i = 0; i < 4; i++)
        {
            frames.add(new TextureRegion(getTexture(), i * 16, 0, 16, 16));
        }
        broRun = new Animation(0.1f, frames);

        frames.clear();

        for(int i = 4; i < 6; i++)
        {
            frames.add(new TextureRegion(getTexture(), i * 16, 0, 16, 16));
        }
        broJump = new Animation(0.1f, frames);


        defineBros();
        broStand = new TextureRegion(getTexture(), 0, 0, 16, 16);
        setBounds(0, 0, 16 / SuperBros.PPM, 16 / SuperBros.PPM);
        setRegion(broStand);
    }

    public State getPlayerState()
    {

        if(box2Bod.getLinearVelocity().y > 0 || (box2Bod.getLinearVelocity().y < 0 && prevState == State.JUMPING))
        {
            return State.JUMPING;
        }

        if(box2Bod.getLinearVelocity().y < 0)
        {
            return State.FALLING;
        }

        else if(box2Bod.getLinearVelocity().x != 0)
        {
            return State.RUNNING;
        }

        else
        {
            return State.STANDING;
        }



    }

    public TextureRegion getFrameState(float dt)
    {
        currState = getPlayerState();

        TextureRegion region;

        switch (currState)
        {
            case JUMPING:
                region = broJump.getKeyFrame(stateTimer);
                break;
            case RUNNING:
                region = broRun.getKeyFrame(stateTimer, true);
                break;
            case FALLING:
                region = broStand;
                break;
            case STANDING:
                region = broStand;
                break;
            default:
                region = broStand;
                break;
        }

        if((box2Bod.getLinearVelocity().x < 0 || !runRight) && !region.isFlipX())
        {
            region.flip(true, false);
            runRight = false;
        }

        else if((box2Bod.getLinearVelocity().x > 0 || runRight) && region.isFlipX())
        {
            region.flip(true, false);
            runRight = true;
        }

        stateTimer = currState == prevState ? stateTimer + dt : 0;
        prevState = currState;

        return region;
    }

    public void update(float dt)
    {
        setPosition(box2Bod.getPosition().x - getWidth() / 2, box2Bod.getPosition().y - getHeight() / 2);
        setRegion(getFrameState(dt));
    }

    public void defineBros()
    {
        BodyDef bDef = new BodyDef();
        bDef.position.set(32 / SuperBros.PPM, 32 / SuperBros.PPM);
        bDef.type =  BodyDef.BodyType.DynamicBody;
        box2Bod = world.createBody(bDef);

        FixtureDef fDef = new FixtureDef();
        CircleShape circle = new CircleShape();
        circle.setRadius(6 / SuperBros.PPM);

        fDef.filter.categoryBits = SuperBros.broBit;
        fDef.filter.maskBits = SuperBros.defBit | SuperBros.coinsBit | SuperBros.bricksBit  | SuperBros.enemiesBit | SuperBros.objectBit | SuperBros.enemiesHeadBit;

        fDef.shape = circle;
        box2Bod.createFixture(fDef);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2/ SuperBros.PPM, 7 / SuperBros.PPM), new Vector2(2/ SuperBros.PPM, 7 / SuperBros.PPM));

        fDef.shape = head;
        fDef.isSensor = true;
        box2Bod.createFixture(fDef).setUserData("head");

    }

}
