package com.mygdx.game.superbros.ManageAssets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.superbros.HUD;
import com.mygdx.game.superbros.Screens.PlayScreen;
import com.mygdx.game.superbros.SuperBros;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class Coins extends InteractableTileObjects
{
    private static TiledMapTileSet sets;
    private final int NullCOIN = 28;
    public Coins(PlayScreen playScreen, Rectangle bounds)
    {
        super(playScreen, bounds);
        fixture.setUserData(this);
        sets = map.getTileSets().getTileSet("tileset_gutter");
        setCatFilter(SuperBros.coinsBit);
    }

    @Override
    public void headHit() {
        Gdx.app.log("Coins", "Collison");
        getCellSpace().setTile(sets.getTile(NullCOIN));
        HUD.addToScore(200);
    }
}
