package com.mygdx.game.superbros.ManageAssets;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.superbros.Screens.PlayScreen;

/**
 * Created by Kalen Wood-Wardlow.
 */

public abstract class Enemies extends Sprite
{
    public Vector2 velocity;
    protected World world;
    protected PlayScreen screen;
    public Body enemyBody;

    public Enemies(PlayScreen playScreen, float x, float y)
    {
        this.world = playScreen.getTheWorld();
        this.screen = playScreen;
        setPosition(x,y);
        defineEnemies();
        velocity = new Vector2(1,0);
        enemyBody.setActive(false);
    }

    protected abstract void defineEnemies();
    public abstract void onHeadHit();

    public abstract void update(float dt);

    public void reverseVelocity(boolean x, boolean y)
    {
        if(x)
        {
            velocity.x = -velocity.x;
        }
        if(y)
        {
            velocity.y = -velocity.y;
        }
    }
}
