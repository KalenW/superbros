package com.mygdx.game.superbros.ManageAssets;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.superbros.Screens.PlayScreen;
import com.mygdx.game.superbros.SuperBros;

/**
 * Created by Kalen Wood-Wardlow.
 */

public abstract class InteractableTileObjects {
    protected World world;
    protected TiledMap map;
    protected TiledMapTile tile;
    protected Rectangle bounds;
    protected Body body;
    protected Fixture fixture;

    public InteractableTileObjects(PlayScreen playScreen, Rectangle bounds)
    {
        this.world = playScreen.getTheWorld();
        this.map = playScreen.getTheMap();
        this.bounds = bounds;

        BodyDef bodyDef = new BodyDef();
        FixtureDef fDef = new FixtureDef();
        PolygonShape poly = new PolygonShape();

        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set((bounds.getX() + bounds.getWidth() / 2) / SuperBros.PPM, (bounds.getY() + bounds.getHeight() / 2) / SuperBros.PPM);

        body = world.createBody(bodyDef);

        poly.setAsBox((bounds.getWidth() / 2) / SuperBros.PPM, (bounds.getHeight() / 2) / SuperBros.PPM);

        fDef.shape = poly;

        fixture = body.createFixture(fDef);

    }

    public abstract void headHit();

    //Category filter set
    public void setCatFilter(short passBit)
    {
        Filter filter = new Filter();
        filter.categoryBits = passBit;
        fixture.setFilterData(filter);
    }

    public TiledMapTileLayer.Cell getCellSpace()
    {
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
        return layer.getCell((int)(body.getPosition().x * SuperBros.PPM / 16), (int)(body.getPosition().y * SuperBros.PPM / 16));
    }
}
