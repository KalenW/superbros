package com.mygdx.game.superbros.ManageAssets;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.superbros.HUD;
import com.mygdx.game.superbros.Screens.PlayScreen;
import com.mygdx.game.superbros.SuperBros;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class Shroom extends Enemies
{
    private boolean setDestroyed;
    private boolean destroyed;
    //Intialize animation variables
    private float stateTimer;
    private Animation walkingAnimation;
    private Array<TextureRegion> shroomFrames;

    public Shroom(PlayScreen playScreen, float x, float y) {
        super(playScreen, x, y);
        shroomFrames = new Array<TextureRegion>();

        for(int k = 0; k < 2; k++)
        {
            shroomFrames.add(new TextureRegion(playScreen.getAtlas().findRegion("shroom"), k * 16, 0, 16, 16));
        }
        walkingAnimation= new Animation(0.4f, shroomFrames);
        stateTimer = 0;
        setBounds(getX(), getY(), 16 / SuperBros.PPM, 16 / SuperBros.PPM);
        setDestroyed = false;
        destroyed = false;
    }


    @Override
    protected void defineEnemies()
    {
        BodyDef bDef = new BodyDef();
        bDef.position.set(getX(), getY());
        bDef.type =  BodyDef.BodyType.DynamicBody;
        enemyBody = world.createBody(bDef);

        FixtureDef fDef = new FixtureDef();
        CircleShape circle = new CircleShape();
        circle.setRadius(6 / SuperBros.PPM);

        fDef.filter.categoryBits = SuperBros.enemiesBit;
        fDef.filter.maskBits = SuperBros.defBit | SuperBros.coinsBit | SuperBros.bricksBit | SuperBros.enemiesBit | SuperBros.objectBit | SuperBros.broBit;

        fDef.shape = circle;
        enemyBody.createFixture(fDef).setUserData(this);

        //Make head of the shroom
        PolygonShape head = new PolygonShape();
        Vector2[] vert = new Vector2[4];
        vert[0] = new Vector2(-5, 8).scl(1 / SuperBros.PPM);
        vert[1] = new Vector2(5, 8).scl(1 / SuperBros.PPM);
        vert[2] = new Vector2(-3, 3).scl(1 / SuperBros.PPM);
        vert[3] = new Vector2(3, 3).scl(1 / SuperBros.PPM);
        head.set(vert);

        fDef.shape = head;
        fDef.restitution = 0.5f;
        fDef.filter.categoryBits = SuperBros.enemiesHeadBit;
        enemyBody.createFixture(fDef).setUserData(this);
    }

    @Override
    public void onHeadHit() {
        HUD.addToScore(50);
        setDestroyed = true;
    }

    public void draw(Batch batch)
    {
        if(!destroyed || stateTimer < 1)
        {
            super.draw(batch);
        }
    }

    //dt = delta time
    public void update(float dt)
    {
        stateTimer += dt;

        if(setDestroyed && !destroyed)
        {
            world.destroyBody(enemyBody);
            destroyed = true;
            setRegion(new TextureRegion(screen.getAtlas().findRegion("shroom"), 32, 0, 16, 16));
            stateTimer = 0;
        }
        else if(!destroyed)
        {
            enemyBody.setLinearVelocity(velocity);
            setPosition(enemyBody.getPosition().x - getWidth() / 2, enemyBody.getPosition().y - getHeight() / 2);
            setRegion(walkingAnimation.getKeyFrame(stateTimer, true));
        }

    }
}
