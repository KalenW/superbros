package com.mygdx.game.superbros.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.superbros.GameController;
import com.mygdx.game.superbros.HUD;
import com.mygdx.game.superbros.ManageAssets.Bros;
import com.mygdx.game.superbros.ManageAssets.Enemies;
import com.mygdx.game.superbros.SuperBros;
import com.mygdx.game.superbros.WorldCreator.Box2WorldCreator;
import com.mygdx.game.superbros.WorldCreator.WorldContactListener;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class PlayScreen implements Screen {
    //Reference to the game itself, used to set screens
    private SuperBros game;
    private TextureAtlas atlas;

    //playscreen variables
    private OrthographicCamera gameCam; //Camera to follow player
    private Viewport gamePort; //Used to maintain the aspect ratio across devices
    private HUD hud; //Creates a HUD to display information to player

    //tiled map variables
    private TmxMapLoader maploader; //Used to load map into the game
    private TiledMap map; //Reference to the map itself
    private OrthogonalTiledMapRenderer renderer; // Used to render map to the screen

    //Box2D variables
    private World world;
    private Box2WorldCreator creator;

    //Character variables
    private Bros player;

    //GameController variable
    GameController gameController;

    public PlayScreen(SuperBros game) {
        atlas = new TextureAtlas("Bro_and_Enemies.pack");

        this.game = game;

        //Creates camera to follow player through the world
        gameCam = new OrthographicCamera();

        //Creates a FitViewport to maintain the correct virtual aspect ratio of the screen
        gamePort = new FitViewport(SuperBros.V_WIDTH / SuperBros.PPM, SuperBros.V_HEIGHT / SuperBros.PPM, gameCam);

        //Create the HUD to display game information
        hud = new HUD(game.batch);

        //load map & map renderer into game
        maploader = new TmxMapLoader();
        map = maploader.load("MAP_level1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / SuperBros.PPM);

        gameCam.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

        world = new World(new Vector2(0,-10), true);

        creator = new Box2WorldCreator(this);

        //create player
        player = new Bros(this);

        gameController = new GameController();

        world.setContactListener(new WorldContactListener());
    }

    public TextureAtlas getAtlas()
    {
        return atlas;
    }

    @Override
    public void show() {

    }

    public void handleInput(float dt) {
        //Handle keyboard inputs
       if(Gdx.input.isKeyJustPressed(Input.Keys.UP))
       {
           player.box2Bod.applyLinearImpulse(new Vector2(0, 4f), player.box2Bod.getWorldCenter(), true);
       }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) && player.box2Bod.getLinearVelocity().x <= 2)
        {
            player.box2Bod.applyLinearImpulse(new Vector2(0.1f, 0), player.box2Bod.getWorldCenter(), true);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT) && player.box2Bod.getLinearVelocity().x >= -2)
        {
            player.box2Bod.applyLinearImpulse(new Vector2(-0.1f, 0), player.box2Bod.getWorldCenter(), true);
        }

        //Handle gameController inputs
        if(gameController.isRIGHT() && player.box2Bod.getLinearVelocity().x <= 2)
        {
            player.box2Bod.applyLinearImpulse(new Vector2(0.1f, 0), player.box2Bod.getWorldCenter(), true);
        }
        if(gameController.isLEFT() && player.box2Bod.getLinearVelocity().x >= -2)
        {
            player.box2Bod.applyLinearImpulse(new Vector2(-0.1f, 0), player.box2Bod.getWorldCenter(), true);
        }
        if(gameController.isJUMP() && player.box2Bod.getLinearVelocity().y == 0)
        {
            player.box2Bod.applyLinearImpulse(new Vector2(0, 4f), player.box2Bod.getWorldCenter(), true);
        }
    }

    public void update(float dt) {
        //Handles user's input
        handleInput(dt);
        //Phsyics simulation in time
        world.step(1/60f, 6, 2);

        hud.update(dt);
        player.update(dt);
        for(Enemies enemies : creator.getShrooms())
        {
            enemies.update(dt);
            if(enemies.getX() < player.getX() + 224 / SuperBros.PPM)
            {
                enemies.enemyBody.setActive(true);
            }
        }
        gameCam.position.x = player.box2Bod.getPosition().x;

        gameCam.update();
        renderer.setView(gameCam);

    }

    @Override
    public void render(float delta) {
        update(delta);



        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderer.render(); //Render map

        //render box2DDebugLines
        //boxDebugRender.render(world, gameCam.combined);

        //if(Gdx.app.getType() == Application.ApplicationType.Android) {
            gameController.draw();
        //}

        //render player
        game.batch.setProjectionMatrix(gameCam.combined);
        game.batch.begin();
        player.draw(game.batch);
        //Draw Enemies
        for(Enemies enemies : creator.getShrooms())
        {
            enemies.draw(game.batch);
        }

        game.batch.end();


        //set batch so camera can see it
        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
    }

    public TiledMap getTheMap()
    {
        return map;
    }

    public World getTheWorld()
    {
        return world;
    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
        gameController.resizeController(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
        world.dispose();
        hud.dispose();
    }
}
