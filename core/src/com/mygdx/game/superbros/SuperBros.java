package com.mygdx.game.superbros;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.superbros.Screens.PlayScreen;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class SuperBros extends Game {
	public static final int V_WIDTH = 400;
	public static final int V_HEIGHT = 208;
	public static final float PPM = 100;
    //Define Collision Bits
    public static final short defBit = 1; //The Default bit
    public static final short broBit = 2;
    public static final short bricksBit = 4;
    public static final short coinsBit = 8;
    public static final short destroyBit = 16;
    public static final short objectBit = 32;
    public static final short enemiesBit = 64;
    public static final short enemiesHeadBit = 128;

	public static SpriteBatch batch;

	
	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen(new PlayScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
