package com.mygdx.game.superbros.WorldCreator;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.superbros.Screens.PlayScreen;
import com.mygdx.game.superbros.ManageAssets.Bricks;
import com.mygdx.game.superbros.ManageAssets.Coins;
import com.mygdx.game.superbros.ManageAssets.Shroom;
import com.mygdx.game.superbros.SuperBros;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class Box2WorldCreator
{
    private Array<Shroom> shrooms;

    public Box2WorldCreator(PlayScreen playScreen)
    {
        World world = playScreen.getTheWorld();
        TiledMap map = playScreen.getTheMap();
        BodyDef bodyD = new BodyDef();
        PolygonShape poly = new PolygonShape();
        FixtureDef fixD = new FixtureDef();
        Body body;

        //Intializes the collison with the ground
        for(MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyD.type = BodyDef.BodyType.StaticBody;
            bodyD.position.set((rect.getX() + rect.getWidth() / 2) / SuperBros.PPM, (rect.getY() + rect.getHeight() / 2) / SuperBros.PPM);

            body = world.createBody(bodyD);

            poly.setAsBox((rect.getWidth() / 2) / SuperBros.PPM, (rect.getHeight() / 2) / SuperBros.PPM);

            fixD.shape = poly;

            body.createFixture(fixD);
        }

        //Intialize the pipes
        for(MapObject object : map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bodyD.type = BodyDef.BodyType.StaticBody;
            bodyD.position.set((rect.getX() + rect.getWidth() / 2) / SuperBros.PPM, (rect.getY() + rect.getHeight() / 2) / SuperBros.PPM);

            body = world.createBody(bodyD);

            poly.setAsBox((rect.getWidth() / 2) / SuperBros.PPM, (rect.getHeight() / 2) / SuperBros.PPM);

            fixD.shape = poly;
            fixD.filter.categoryBits = SuperBros.objectBit;

            body.createFixture(fixD);
        }

        //Intialize the bricks
        for(MapObject object : map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

           new Bricks(playScreen, rect);
        }

        //Intialize Coins
        for(MapObject object : map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

           new Coins(playScreen, rect);
        }

        //Intialize Shroom Enemies
        shrooms = new Array<Shroom>();
        for(MapObject object : map.getLayers().get(6).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            shrooms.add(new Shroom(playScreen, rect.getX() / SuperBros.PPM, rect.getY() / SuperBros.PPM));
        }
    }

    public Array<Shroom> getShrooms()
    {
        return shrooms;
    }
}
