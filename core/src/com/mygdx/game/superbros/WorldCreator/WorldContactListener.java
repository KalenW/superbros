package com.mygdx.game.superbros.WorldCreator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.mygdx.game.superbros.ManageAssets.Enemies;
import com.mygdx.game.superbros.ManageAssets.InteractableTileObjects;
import com.mygdx.game.superbros.SuperBros;

/**
 * Created by Kalen Wood-Wardlow.
 */

public class WorldContactListener implements ContactListener {

    /*
     * Implementened method from ContactListener
     */
    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();
        //Defines the collsion bits
        int defC =  fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

        if(fixA.getUserData() == "head" || fixB.getUserData() == "head")
        {
            Fixture head = fixA.getUserData() == "head" ? fixA : fixB;
            Fixture obj = head == fixA  ? fixB : fixA;

            if(obj.getUserData() != null && InteractableTileObjects.class.isAssignableFrom(obj.getUserData().getClass()))
            {
                ((InteractableTileObjects) obj.getUserData()).headHit();
            }
        }

        switch(defC)
        {
            case SuperBros.enemiesHeadBit | SuperBros.broBit:
                if(fixA.getFilterData().categoryBits == SuperBros.enemiesHeadBit)
                {
                    ((Enemies)fixA.getUserData()).onHeadHit();
                }
                else if(fixA.getFilterData().categoryBits == SuperBros.enemiesHeadBit)
                {
                    ((Enemies)fixB.getUserData()).onHeadHit();
                }
                break;

            case SuperBros.enemiesBit | SuperBros.objectBit:
                if(fixA.getFilterData().categoryBits == SuperBros.enemiesBit)
                {
                    ((Enemies)fixA.getUserData()).reverseVelocity(true, false);
                }
                else
                {
                    ((Enemies)fixB.getUserData()).reverseVelocity(true, false);
                }
                break;

            case SuperBros.broBit | SuperBros.enemiesBit:
                Gdx.app.log("Bro", "Died");
                break;

            case SuperBros.enemiesBit | SuperBros.enemiesBit:
                ((Enemies)fixA.getUserData()).reverseVelocity(true, false);
                ((Enemies)fixB.getUserData()).reverseVelocity(true, false);
                break;

        }
    }

    @Override
    public void endContact(Contact contact) {


    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
